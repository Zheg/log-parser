#include <iostream>
#include <cstdio>
#include "string.h"

const int CHAR_SIZE = sizeof(char);
const int MAX_LENGTH = 10;
const int COL_COUNT = 15;

double array[MAX_LENGTH];
int arrayInt[MAX_LENGTH];
char *arrayChar[MAX_LENGTH];
int countEl, countCol;
char endOfArray;
char errorMessage[500];

void readAndWriteArrayDouble(FILE* &in, FILE* &out, int format) {
    countEl = 0;
    endOfArray = '[';
    while (endOfArray != ']') {
        fscanf(in, "%lf%c", &array[countEl++], &endOfArray);
    }
    if (format == 1) {
        for (int i = 0; i < countEl - 1; ++i) {
            fprintf(out, "%.1lf, ", array[i]);
        }
        fprintf(out, "%.1lf;", array[countEl - 1]);
    } else if (format == 3){
        for (int i = 0; i < countEl - 1; ++i) {
            fprintf(out, "%.3lf, ", array[i]);
        }
        fprintf(out, "%.3lf;", array[countEl - 1]);
    }
}

void readAndWriteArrayInt(FILE* &in, FILE* &out) {
    countEl = 0;
    endOfArray = '[';
    while (endOfArray != ']') {
        fscanf(in, "%i%c", &arrayInt[countEl++], &endOfArray);
    }
    for (int i = 0; i < countEl - 1; ++i) {
        fprintf(out, "%i, ", arrayInt[i]);
    }
    fprintf(out, "%i;", arrayInt[countEl - 1]);
}

void readAndWriteArrayString(FILE* &in, FILE* &out) {
    countEl = 0;
    endOfArray = '[';
    char tmp = '1';
    while (endOfArray != ']') {
        tmp = '1';
        fseek(in, CHAR_SIZE, SEEK_CUR);
        fprintf(out, "%c", '"');
        while (tmp != '"') {
            fscanf(in, "%c", &tmp);
            fprintf(out, "%c", tmp);
        }
        fscanf(in, "%c", &endOfArray);
        if (endOfArray == ']'){
            fprintf(out, ";");
        } else {
            fprintf(out, ", ");
        }
    }
}

int main(int argc, char *argv[]) {
    FILE *in;
    FILE *out;
    if (argc > 1)
        in = fopen(argv[1], "r");
    else {
        std::cout << "Not enough arguments";
        return 0;
    }
    if (argc > 2 && argv[2] == "append"){
        out = fopen("out.csv", "a");
    } else {
        out = fopen("out.csv", "w");
        fprintf(out, "id;ts;mid;vbat;usb;a;g;m;p;ref;a;dd;rs;qf;error message\n");
    }
    int id, mid, p, ma[MAX_LENGTH], g[MAX_LENGTH], m[MAX_LENGTH], ref, qf[MAX_LENGTH], countRow = 0, usb;
    double ts, dd[MAX_LENGTH], rs[MAX_LENGTH], aa[MAX_LENGTH], vbat;
    char mod = '1';
    while (!feof(in)){
        ++countRow;
        countCol = 0;
        fscanf(in, "%c", &mod);
        if (mod == '{') {
            fseek(in, 6 * CHAR_SIZE, SEEK_CUR);
            fscanf(in, "%x", &id);
            fprintf(out, "0x%x;", id);
            ++countCol;
            fseek(in, 3 * CHAR_SIZE, SEEK_CUR);
            fscanf(in, "%c", &mod);
            if (mod == 't') {
                fseek(in, 4 * CHAR_SIZE, SEEK_CUR);
                fscanf(in, "%lf", &ts);
                fprintf(out, "%4.4lf;", ts);
                ++countCol;
                fseek(in, 8 * CHAR_SIZE, SEEK_CUR);
            } else {
                fprintf(out, "-;");
                ++countCol;
                fseek(in, 4 * CHAR_SIZE, SEEK_CUR);
            }
            fscanf(in, "%i", &mid);
            fprintf(out, "%i;", mid);
            ++countCol;
            fseek(in, 2 * CHAR_SIZE, SEEK_CUR);
            fscanf(in, "%c", &mod);
            if (mod == 'a' || mod == 'v') {
                if (mod == 'v') {
                    fseek(in, 6 * CHAR_SIZE, SEEK_CUR);
                    fscanf(in, "%lf", &vbat);
                    fprintf(out, "%.2lf;", vbat);
                    ++countCol;
                    fseek(in, 8 * CHAR_SIZE, SEEK_CUR);
                    fscanf(in, "%i", &usb);
                    fprintf(out, "%i;", usb);
                    ++countCol;
                    fseek(in, 3 * CHAR_SIZE, SEEK_CUR);
                } else {
                    for (int i = 0; i < 2; ++i) {
                        fprintf(out, "-;");
                        ++countCol;
                    }
                }
                fseek(in, 4 * CHAR_SIZE, SEEK_CUR);
                readAndWriteArrayDouble(in, out, 3);
                ++countCol;
                fseek(in, 8 * CHAR_SIZE, SEEK_CUR);
                readAndWriteArrayDouble(in, out, 1);
                ++countCol;
                fseek(in, 1 * CHAR_SIZE, SEEK_CUR);
                fscanf(in, "%c", &mod);
                if (mod == ',') {
                    fseek(in, 5 * CHAR_SIZE, SEEK_CUR);
                    readAndWriteArrayInt(in, out);
                    ++countCol;
                    fseek(in, 5 * CHAR_SIZE, SEEK_CUR);
                    fscanf(in, "%i", &p);
                    fprintf(out, "%i;", p);
                    ++countCol;
                    fscanf(in, "%c", &mod);
                }
                while (countCol < COL_COUNT) {
                    fprintf(out, "-;");
                    ++countCol;
                }
                fscanf(in, "%c", &mod);
                fprintf(out, "\n");
            } else if (mod == 'm') {
                for (int i = 0; i < 6; ++i) {
                    fprintf(out, "-;");
                    ++countCol;
                }
                fseek(in, 13 * CHAR_SIZE, SEEK_CUR);
                fscanf(in, "%i", &ref);
                fprintf(out, "%i;", ref);
                ++countCol;
                fseek(in, 13 * CHAR_SIZE, SEEK_CUR);
                readAndWriteArrayString(in, out);
                ++countCol;
                fseek(in, 7 * CHAR_SIZE, SEEK_CUR);
                readAndWriteArrayDouble(in, out, 3);
                ++countCol;
                fseek(in, 7 * CHAR_SIZE, SEEK_CUR);
                readAndWriteArrayDouble(in, out, 1);
                ++countCol;
                fseek(in, 7 * CHAR_SIZE, SEEK_CUR);
                readAndWriteArrayInt(in, out);
                ++countCol;
                fscanf(in, "%c%c%c", &mod, &mod, &mod);
                while (countCol < COL_COUNT) {
                    fprintf(out, "-;");
                    ++countCol;
                }
                fprintf(out, "\n");
            } else {
                fprintf(out, "Wrong format in row %i", countRow);
                fclose(in);
                fclose(out);
                std::cout << "Wrong format in row " << countRow;
                return 0;
            }
        /*} else if (mod == '$') {
//            for (int i = 0; i < COL_COUNT; ++i){
//                fprintf(out, "END;");
//            }
            fclose(in);
            fclose(out);
            return 0;*/
        } else {
            for (int i = 0; i < COL_COUNT - 1; ++i){
                fprintf(out, "error;");
            }
            fgets(errorMessage, 500, in);
            fprintf(out, "%c%s", mod, errorMessage);
        }
    }
    fclose(in);
    fclose(out);
    return 0;
}